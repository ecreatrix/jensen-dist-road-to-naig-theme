/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./roadtonaig/src/scripts/app.jsx":
/*!****************************************!*\
  !*** ./roadtonaig/src/scripts/app.jsx ***!
  \****************************************/
/***/ (function() {

$ = jQuery.noConflict();
$(document).on('ready', function () {
  $('p > img').unwrap();
  $('p:empty').remove();
});

function updateButton(el, endpoint) {
  var data = {
    post_id: el.data('post-id'),
    user_id: el.data('user-id'),
    label: el.data('label'),
    points: el.data('points'),
    submember: el.data('submember'),
    type: el.data('type')
  };
  el.html('<i class="fas fa-spinner fa-pulse"></i>');
  jQuery('.register-registered_workshops:not(.complete)').addClass('pending');
  jQuery('.register-completed_activities:not(.complete)').addClass('pending');
  jQuery('.register-completed_training:not(.complete)').addClass('pending');
  wp.apiFetch({
    //path: "/wp/v2/featured-post"
    path: "/jensen/v1/".concat(endpoint),
    method: 'POST',
    data: data
  }).then(function (result) {
    //console.log(result)
    if (result) {
      el.removeClass('incomplete');
      el.addClass('complete');
      el.text(result);
      jQuery('.register-registered_workshops:not(.complete)').removeClass('pending');
      jQuery('.register-completed_activities:not(.complete)').removeClass('pending');
      jQuery('.register-completed_training:not(.complete)').removeClass('pending');
    }
  });
}

jQuery('.register-registered_workshops:not(.complete)').bind('click', function () {
  updateButton(jQuery(this), 'workshop/register');
});
jQuery('.register-completed_activities:not(.complete)').bind('click', function () {
  updateButton(jQuery(this), 'activity/register');
});
jQuery('.register-completed_training:not(.complete)').bind('click', function () {
  updateButton(jQuery(this), 'training/register');
});
$(document).ready(function () {
  var hash = window.location.hash;

  if (hash != "" && $(hash).length) {
    $(hash + '> .collapse').collapse('show');
  }
});

/***/ }),

/***/ "./roadtonaig/src/styles/admin.scss":
/*!******************************************!*\
  !*** ./roadtonaig/src/styles/admin.scss ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./roadtonaig/src/styles/app.scss":
/*!****************************************!*\
  !*** ./roadtonaig/src/styles/app.scss ***!
  \****************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./roadtonaig/src/styles/editor.scss":
/*!*******************************************!*\
  !*** ./roadtonaig/src/styles/editor.scss ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/roadtonaig/assets/scripts/app": 0,
/******/ 			"roadtonaig/assets/styles/editor": 0,
/******/ 			"roadtonaig/assets/styles/app": 0,
/******/ 			"roadtonaig/assets/styles/admin": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkjg_road_to_naig"] = self["webpackChunkjg_road_to_naig"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["roadtonaig/assets/styles/editor","roadtonaig/assets/styles/app","roadtonaig/assets/styles/admin"], function() { return __webpack_require__("./roadtonaig/src/scripts/app.jsx"); })
/******/ 	__webpack_require__.O(undefined, ["roadtonaig/assets/styles/editor","roadtonaig/assets/styles/app","roadtonaig/assets/styles/admin"], function() { return __webpack_require__("./roadtonaig/src/styles/admin.scss"); })
/******/ 	__webpack_require__.O(undefined, ["roadtonaig/assets/styles/editor","roadtonaig/assets/styles/app","roadtonaig/assets/styles/admin"], function() { return __webpack_require__("./roadtonaig/src/styles/app.scss"); })
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["roadtonaig/assets/styles/editor","roadtonaig/assets/styles/app","roadtonaig/assets/styles/admin"], function() { return __webpack_require__("./roadtonaig/src/styles/editor.scss"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;