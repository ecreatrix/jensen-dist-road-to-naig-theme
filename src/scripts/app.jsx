$ = jQuery.noConflict()

$( document ).on( 'ready', function() {
	$( 'p > img' ).unwrap()
	$( 'p:empty' ).remove()
} )

function updateButton(el, endpoint) {
    var data = {
        post_id: el.data('post-id'),
        user_id: el.data('user-id'),
        label: el.data('label'),
        points: el.data('points'),
        submember: el.data('submember'),
        type: el.data('type'),
    }

    el.html('<i class="fas fa-spinner fa-pulse"></i>')
    jQuery('.register-registered_workshops:not(.complete)').addClass('pending')
    jQuery('.register-completed_activities:not(.complete)').addClass('pending')
    jQuery('.register-completed_training:not(.complete)').addClass('pending')

    wp.apiFetch({
        //path: "/wp/v2/featured-post"
        path: `/jensen/v1/${ endpoint }`,
        method: 'POST',
        data,
    }).then( result => {
        //console.log(result)
        if( result ) {
            el.removeClass('incomplete')
            el.addClass('complete')
            el.text(result)

            jQuery('.register-registered_workshops:not(.complete)').removeClass('pending')
            jQuery('.register-completed_activities:not(.complete)').removeClass('pending')
            jQuery('.register-completed_training:not(.complete)').removeClass('pending')
        }
    })
}

jQuery('.register-registered_workshops:not(.complete)').bind('click', function() {
    updateButton(jQuery(this), 'workshop/register')
})

jQuery('.register-completed_activities:not(.complete)').bind('click', function() {
    updateButton(jQuery(this), 'activity/register')
})

jQuery('.register-completed_training:not(.complete)').bind('click', function() {
    updateButton(jQuery(this), 'training/register')
});

$(document).ready(function() {
    let hash = window.location.hash
    if (hash != "" && $(hash).length ) {
        $(hash + '> .collapse').collapse('show');
    }
});