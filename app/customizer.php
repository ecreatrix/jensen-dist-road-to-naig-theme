<?php
namespace jg\Theme;

use WP_Customize_Manager;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Customizer::class ) ) {
	class Customizer {
		public function __construct() {
			add_action( 'customize_register', [$this, 'logos'] );
			add_action( 'customize_register', [$this, 'social_media'] );
			add_action( 'customize_register', [$this, 'games_fields'] );
			add_action( 'customize_register', [$this, 'profile_fields'] );
			add_action( 'customize_register', [$this, 'panels'] );

		}

		function games_fields( \WP_Customize_Manager $wp_customize ) {
			$wp_customize->add_setting(
				'jg_points_off-ice', [] );
			$wp_customize->add_control(
				'jg_points_off-ice', [
					'label'   => esc_html__( 'Off-Ice Training', 'jg_theme' ),
					'section' => 'jg_info',
					'type'    => 'number',
				] );

			$wp_customize->add_setting(
				'jg_points_challenges', [] );
			$wp_customize->add_control(
				'jg_points_challenges', [
					'label'   => esc_html__( 'Challenges', 'jg_theme' ),
					'section' => 'jg_info',
					'type'    => 'number',
				] );

			$wp_customize->add_setting(
				'jg_points_dryland', [] );
			$wp_customize->add_control(
				'jg_points_dryland', [
					'label'   => esc_html__( 'Dryland Training', 'jg_theme' ),
					'section' => 'jg_info',
					'type'    => 'number',
				] );

			$wp_customize->add_setting(
				'jg_points_fitness_training', [] );
			$wp_customize->add_control(
				'jg_points_fitness_training', [
					'label'   => esc_html__( 'Fitness Training', 'jg_theme' ),
					'section' => 'jg_info',
					'type'    => 'number',
				] );

			$wp_customize->add_setting(
				'jg_game_edition', []
			);
			$wp_customize->add_control(
				'jg_game_edition', [
					'label'    => esc_html__( 'Games Edition', 'jg_theme' ),
					'section'  => 'jg_info',
					'type'     => 'select',
					'choices'  => [
						'summer_2021' => __( 'Summer 2021' ),
						'fall_2021'   => __( 'Fall 2021' ),
					],
					'priority' => 80,
				] );

			$wp_customize->add_setting(
				'jg_game_start', []
			);
			$wp_customize->add_control(
				'jg_game_start', [
					'label'    => esc_html__( 'Start Date', 'jg_theme' ),
					'section'  => 'jg_info',
					'type'     => 'date',
					'priority' => 80,
				] );

			$wp_customize->add_setting(
				'jg_game_end', []
			);
			$wp_customize->add_control(
				'jg_game_end', [
					'label'    => esc_html__( 'End Date', 'jg_theme' ),
					'section'  => 'jg_info',
					'type'     => 'date',
					'priority' => 80,
				] );

			/*$wp_customize->add_setting(
			'jg_points_fitness_initial', [] );
			$wp_customize->add_control(
			'jg_points_fitness_initial', [
			'label'   => esc_html__( 'Initial Fitness Test', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'number',
			] );

			$wp_customize->add_setting(
			'jg_points_fitness_final', [] );
			$wp_customize->add_control(
			'jg_points_fitness_final', [
			'label'   => esc_html__( 'Final Fitness Test', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'number',
			] );

			$wp_customize->add_setting(
			'jg_points_feedback', [] );
			$wp_customize->add_control(
			'jg_points_feedback', [
			'label'   => esc_html__( 'Feedback Survey', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'number',
			] );

			$wp_customize->add_setting(
			'jg_points_training', [] );
			$wp_customize->add_control(
			'jg_points_training', [
			'label'   => esc_html__( 'Training (per day)', 'jg_theme' ),
			'section' => 'jg_info',
			'type'    => 'number',
			] );*/

			$wp_customize->add_setting(
				'jg_points_challenges', [] );
			$wp_customize->add_control(
				'jg_points_challenges', [
					'label'   => esc_html__( 'Challenges', 'jg_theme' ),
					'section' => 'jg_info',
					'type'    => 'number',
				] );

			/*$wp_customize->add_setting(
		'jg_points_workshop_registered', [] );
		$wp_customize->add_control(
		'jg_points_workshop_registered', [
		'label'   => esc_html__( 'Workshop (registered)', 'jg_theme' ),
		'section' => 'jg_info',
		'type'    => 'number',
		] );

		$wp_customize->add_setting(
		'jg_points_workshop_attended', [] );
		$wp_customize->add_control(
		'jg_points_workshop_attended', [
		'label'   => esc_html__( 'Workshop (attended)', 'jg_theme' ),
		'section' => 'jg_info',
		'type'    => 'number',
		] );

		$wp_customize->add_setting(
		'jg_points_sport', [] );
		$wp_customize->add_control(
		'jg_points_sport', [
		'label'   => esc_html__( 'Sport (per week)', 'jg_theme' ),
		'section' => 'jg_info',
		'type'    => 'number',
		] );

		$wp_customize->add_setting(
		'jg_workshop_email', [] );
		$wp_customize->add_control(
		'jg_workshop_email', [
		'label'   => esc_html__( 'Workshop signup email', 'jg_theme' ),
		'section' => 'jg_info',
		'type'    => 'textarea',
		] );*/
		}

		function logos( \WP_Customize_Manager $wp_customize ) {
			$wp_customize->add_setting( 'jg_header_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_header_logo', [
				'label'    => 'Menu Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_header_logo',
				'priority' => 8,
			] ) );

			$wp_customize->add_setting( 'jg_ontario_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_ontario_logo', [
				'label'    => 'Ontario Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_ontario_logo',
				'priority' => 9,
			] ) );

			$wp_customize->add_setting( 'jg_sponsor_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_sponsor_logo', [
				'label'    => 'Sponsor Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_sponsor_logo',
				'priority' => 9,
			] ) );

			$wp_customize->add_setting( 'jg_footer_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_footer_logo', [
				'label'    => 'Footer Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_footer_logo',
				'priority' => 10,
			] ) );

			$wp_customize->add_setting( 'jg_footer_bg' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_footer_bg', [
				'label'    => 'Footer Background',
				'section'  => 'title_tagline',
				'settings' => 'jg_footer_bg',
				'priority' => 25,
			] ) );

			$wp_customize->add_setting( 'jg_signin_page_primary_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_primary_logo', [
				'label'    => 'Sign In Page Road to NAIG Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_signin_page_primary_logo',
				'priority' => 26,
			] ) );
			$wp_customize->add_setting( 'jg_signin_page_secondary_logo' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_secondary_logo', [
				'label'    => 'Sign In Page Well Nation Logo',
				'section'  => 'title_tagline',
				'settings' => 'jg_signin_page_secondary_logo',
				'priority' => 26,
			] ) );
			$wp_customize->add_setting( 'jg_signin_page_bg' );
			$wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'jg_signin_page_bg', [
				'label'    => 'Sign In Page Background',
				'section'  => 'title_tagline',
				'settings' => 'jg_signin_page_bg',
				'priority' => 26,
			] ) );
		}

		function panels( $wp_customize ) {
			// add panel
			// Add Theme Options Panel.
			$wp_customize->add_panel( 'jg_option_panel', [
				'title'      => esc_html__( 'Road to NAIG', 'jg' ),
				'priority'   => 20,
				'capability' => 'edit_theme_options',
			] );

			$wp_customize->add_section( 'jg_social', [
				'title'       => esc_html__( 'Social Media & Contact', 'wp_firstest_news_theme' ),
				'description' => __( 'Contact info and social media links', 'jg_theme' ),
				'priority'    => 110,
				'capability'  => 'edit_theme_options',
				'panel'       => 'jg_option_panel',
			] );

			$wp_customize->add_section( 'jg_profile', [
				'title'       => __( 'User Profile', 'jg' ),
				'description' => sprintf( __( 'User Profile Pages', 'jg' ) ),
				'priority'    => 130,
				'capability'  => 'edit_theme_options',
				'panel'       => 'jg_option_panel',
			] );

			$wp_customize->add_section( 'jg_info', [
				'title'       => __( 'Games Info', 'jg' ),
				'description' => sprintf( __( 'Points', 'jg' ) ),
				'priority'    => 120,
				'capability'  => 'edit_theme_options',
				'panel'       => 'jg_option_panel',
			] );
		}

		function profile_fields( \WP_Customize_Manager $wp_customize ) {
			$wp_customize->add_setting(
				'jg_signin', []
			);
			$wp_customize->add_control(
				'jg_signin', [
					'label'    => esc_html__( 'Sign In Page', 'jg_theme' ),
					'section'  => 'jg_profile',
					'type'     => 'dropdown-pages',
					'priority' => 99,
				] );

			$wp_customize->add_setting(
				'jg_registration_start', []
			);
			$wp_customize->add_control(
				'jg_registration_start', [
					'label'    => esc_html__( 'Registration Form Page', 'jg_theme' ),
					'section'  => 'jg_profile',
					'type'     => 'dropdown-pages',
					'priority' => 99,
				] );

			$wp_customize->add_setting(
				'jg_registration_complete', []
			);
			$wp_customize->add_control(
				'jg_registration_complete', [
					'label'    => esc_html__( 'Registration Completion Page', 'jg_theme' ),
					'section'  => 'jg_profile',
					'type'     => 'dropdown-pages',
					'priority' => 99,
				] );

			$wp_customize->add_setting(
				'jg_profile', []
			);
			$wp_customize->add_control(
				'jg_profile', [
					'label'    => esc_html__( 'Profile Page', 'jg_theme' ),
					'section'  => 'jg_profile',
					'type'     => 'dropdown-pages',
					'priority' => 99,
				] );
		}

		// Social media and business info
		function social_media( \WP_Customize_Manager $wp_customize ) {
			$wp_customize->add_setting(
				'jg_email', [
					'sanitize_callback' => 'sanitize_email', //removes all invalid characters
				] );
			$wp_customize->add_control(
				'jg_email', [
					'label'   => esc_html__( 'Email', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'email',
				] );

			$wp_customize->add_setting(
				'jg_address', [
					'sanitize_callback' => 'wp_filter_nohtml_kses', //removes all HTML from content
				] );
			$wp_customize->add_control(
				'jg_address', [
					'label'   => esc_html__( 'Address', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'textarea',
				] );

			$wp_customize->add_setting(
				'jg_phone', [
					'sanitize_callback' => 'wp_filter_nohtml_kses', //removes all HTML from content
				] );
			$wp_customize->add_control(
				'jg_phone', [
					'label'   => esc_html__( 'Phone Number', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'text',
				] );

			$wp_customize->add_setting(
				'jg_fax', [
					'sanitize_callback' => 'wp_filter_nohtml_kses', //removes all HTML from content
				] );
			$wp_customize->add_control(
				'jg_fax', [
					'label'   => esc_html__( 'Fax Number', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'text',
				] );

			$wp_customize->add_setting(
				'jg_instagram', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'jg_instagram', [
					'label'   => esc_html__( 'Instagram', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'jg_twitter', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'jg_twitter', [
					'label'   => esc_html__( 'Twitter', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'url',
				] );

			$wp_customize->add_setting(
				'jg_facebook', [
					'sanitize_callback' => 'esc_url_raw', //cleans URL from all invalid characters
				] );
			$wp_customize->add_control(
				'jg_facebook', [
					'label'   => esc_html__( 'Facebook', 'jg_theme' ),
					'section' => 'jg_social',
					'type'    => 'url',
				] );
		}
	}

	new Customizer();
}