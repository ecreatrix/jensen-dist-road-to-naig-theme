<?php

/**
 * Theme setup.
 */
namespace jg\Theme;
/*
 * Register the initial theme setup.
 *
 * @return void
 */
add_action( 'after_setup_theme', function () {
    add_theme_support( 'align-wide' );
}, 20 );