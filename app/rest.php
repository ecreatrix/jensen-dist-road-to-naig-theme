<?php
namespace jg\Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Rest::class ) ) {
	class Rest {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'rest_init'] );
		}

		function activity( \WP_REST_Request $request ) {
			return $this->generic( $request, 'completed_activities' );
		}

		function generic( \WP_REST_Request $request, $completion_key ) {
			$points    = $request->get_param( 'points' );
			$user_id   = $request->get_param( 'user_id' );
			$label     = $request->get_param( 'label' );
			$post_id   = $request->get_param( 'post_id' );
			$type      = $request->get_param( 'type' );
			$submember = trim( $request->get_param( 'submember' ) );
			$edition   = \jg\Theme\HelpersUser::get_edition();

			if ( ! $user_id ) {
				return false;
			}

			$meta           = get_user_meta( $user_id );
			$edition_info   = get_user_meta( $user_id, $edition, true );
			$edition_info   = array_merge( \jg\Theme\HelpersUser::member_info(), $edition_info );
			$completed_meta = $edition_info && array_key_exists( $completion_key, $edition_info ) ? $edition_info[$completion_key] : [];

			if ( is_string( $completed_meta ) ) {
				$completed_meta = [];
			}

			$text = 'Registered';
			if ( 'completed_training' === $type ) {
				$text = 'Completed';
			}

			$post_info = ['points' => $points, 'label' => $label];

			$family = \jg\Theme\HelpersUser::get_family( $user_id );
			if ( $family ) {
				// Add post to member - family profile
				$family[$submember][$edition][$completion_key][$post_id] = $post_info;

				$text = $family[$submember]['first_name'] . ' has been registered';

				if ( ! array_key_exists( $post_id, $completed_meta ) ) {
					// New activity, add to profile
					$family[$submember][$edition]['points'] = $family[$submember][$edition]['points'] + $points;
				}

				update_user_meta( $user_id, 'family', $family );
			}

			if ( ! array_key_exists( $completion_key, $edition_info ) || ! array_key_exists( $post_id, $completed_meta ) ) {
				// Add post to user
				$edition_info['points'] = $edition_info['points'] + $points;
			}
			$edition_info[$completion_key][$post_id] = $post_info;

			update_user_meta( $user_id, $edition, $edition_info );

			return $text;
		}

		function rest_init() {
			register_rest_route( 'jensen/v1', '/workshop/attend/', [
				'methods'  => \WP_REST_Server::EDITABLE,
				'callback' => [$this, 'workshop_attend'],
			] );
			register_rest_route( 'jensen/v1', '/workshop/register/', [
				'methods'  => \WP_REST_Server::EDITABLE,
				'callback' => [$this, 'workshop_register'],
			] );
			register_rest_route( 'jensen/v1', '/activity/register/', [
				'methods'  => \WP_REST_Server::EDITABLE,
				'callback' => [$this, 'activity'],
			] );
			register_rest_route( 'jensen/v1', '/training/register/', [
				'methods'  => \WP_REST_Server::EDITABLE,
				'callback' => [$this, 'training'],
			] );
		}

		function training( \WP_REST_Request $request ) {
			return $this->generic( $request, 'completed_training' );
		}

		function workshop_attend( \WP_REST_Request $request ) {
			return $this->generic( $request, 'attended_workshops' );
		}

		function workshop_register( \WP_REST_Request $request ) {
			$text = $this->generic( $request, 'registered_workshops' );

			// Send email on registration
			$post_id = $request->get_param( 'post_id' );
			\jg\Theme\HelpersUser::workshop_registration_email( $post_id );

			return $text;
		}
	}

	new Rest();
}