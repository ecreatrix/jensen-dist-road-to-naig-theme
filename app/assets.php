<?php
namespace jg\Theme;

use function Roots\asset;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Assets::class ) ) {
    class Assets {
        public function __construct() {
            add_action( 'wp_enqueue_scripts', [$this, 'frontend'] );

            add_action( 'enqueue_block_editor_assets', [$this, 'editor'] );
            add_action( 'wp_enqueue_scripts', [$this, 'editor'] );

            add_action( 'admin_enqueue_scripts', [$this, 'admin'], 999 );
            add_action( 'login_enqueue_scripts', [$this, 'admin'], 999 );
        }

        // Enqueue local and remote assets
        public function add_assets( $assets = false, $type = false ) {
            if ( ! $assets || ! is_array( $assets ) ) {
                return;
            }

            foreach ( $assets as $item ) {
                // Name required, skip if it is not present
                if ( ! array_key_exists( 'name', $item ) ) {
                    continue;
                }

                $name  = $item['name'];
                $asset = null;

                // Figure out if it's a script or a style
                $script = false;
                $style  = false;
                if ( 'js' === $type || strpos( $name, '.js' ) !== false ) {
                    $script = true;
                } else if ( 'css' === $type || strpos( $name, '.css' ) !== false ) {
                    $style = true;
                }

                // Get local file or remote link
                if ( array_key_exists( 'link', $item ) && strpos( $item['link'], 'http' ) !== false ) {
                    $asset = $item['link'];
                } else if ( $style ) {
                    $asset = asset( 'styles/' . $item['name'] . '.css' );
                } else if ( $script ) {
                    $asset = asset( 'scripts/' . $item['name'] . '.js' );
                }

                // Set version for cache busting
                $version = null;
                if ( ! is_string( $asset ) && file_exists( $asset->path() ) ) {
                    $version = filemtime( $asset->path() );
                    $uri     = $asset->uri();
                } else if ( array_key_exists( 'link', $item ) ) {
                    $uri = $item['link'];
                } else {
                    return;
                }

                $dependencies = [];
                if ( array_key_exists( 'dependencies', $item ) && is_array( $item['dependencies'] ) ) {
                    $dependencies = $item['dependencies'];
                }

                // Add prefix before name
                $name = 'jg-' . $name;
                if ( array_key_exists( 'prefix', $item ) ) {
                    $name = $item['prefix'] . $name;
                }

                // Add parameters inline for use by scripts
                if ( array_key_exists( 'script_object', $item ) && array_key_exists( 'parameters', $item ) ) {
                    wp_localize_script( $name, $item['script_object'], $item['parameters'] );
                }

                $in_footer = array_key_exists( 'in_footer', $item ) ? $item['in_footer'] : true;

                // Enqueue file/link
                if ( $script ) {
                    wp_enqueue_script( $name, $uri, $dependencies, $version, $in_footer );
                } else if ( $style ) {
                    wp_enqueue_style( $name, $uri, $dependencies, $version );
                }
            }
        }

        public function admin() {
            $styles = [
                [
                    'name' => 'admin',
                ],
                $this->font_fontawesome(),
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function bootstrap( $bundle = false ) {
            $name = 'library/bootstrap';
            if ( $bundle ) {
                $name .= '.bundle';
            }

            $script = [
                'name'         => $name . '.min',
                'dependencies' => ['jquery'],
            ];

            return $script;
        }

        public function editor() {
            $styles = [
                [
                    'name' => 'editor',
                ],
                $this->font_fontawesome(),
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function font_fontawesome() {
            $google = [
                'name' => 'fontawesome',
                'link' => 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css',
            ];

            return $google;
        }

        public function font_google() {
            $google = [
                'name' => 'google-font',
                'link' => 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;500;700;800&family=Open+Sans:ital,wght@0,400;0,600;1,400&display=swap',
            ];

            return $google;
        }

        public function frontend() {
            if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

            $scripts = [
                'main'      => [
                    'name'         => 'app',
                    'dependencies' => ['jquery', 'jg-library/bootstrap.bundle.min'],
                ],
                'header'    => [
                    'name'         => 'header-scripts',
                    'dependencies' => ['jquery'],
                    'in_footer'    => false,
                ],
                'bootstrap' => $this->bootstrap( true ),
            ];

            $this->add_assets( $scripts, 'js' );

            $styles = [
                [
                    'name' => 'app',
                ],
                $this->font_fontawesome(),
                $this->font_google(),
            ];

            $this->add_assets( $styles, 'css' );
        }

        public function popper() {
            $script = [
                'name' => 'library/popper.min',
            ];

            return $script;
        }
    }

    new Assets();
}