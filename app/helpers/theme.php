<?php
namespace jg\Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( HelpersTheme::class ) ) {
	class HelpersTheme {
		public function __construct() {}
		public static function mod_image( $key, $inline = false, $atts = '' ) {
			$id = get_theme_mod( $key );
			if ( $id ) {
				if ( $inline ) {
					return 'background-image: url(\'' . $id . '\');';
				}

				// Some theme mods give the image url instead of id
				$url_to_id = attachment_url_to_postid( $id );
				if ( $url_to_id ) {
					$id = $url_to_id;
				}

				return wp_get_attachment_image( $id, 'full', false, $atts );
			}

			return false;
		}

		static function theme_page( $post_id, $theme_page ) {
			$query_user_id = isset( $_GET['user_id'] ) ? $_GET['user_id'] : false;
			if ( ! $post_id ) {
				$post_id = get_queried_object_id();
			}

			if ( 'profile' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_profile' );
			} else if ( 'registration_complete' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_registration_complete' );
			} else if ( 'registration_start' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_registration_start' );
			} else if ( 'signin' === $theme_page ) {
				$theme_page = get_theme_mod( 'jg_signin' );
			}

			return [
				'page_id'           => $theme_page,
				'permalink'         => get_permalink( $theme_page ) . $query_user_id,
				'current_page'      => $theme_page == $post_id,
				'add_query_user'    => $query_user_id ? '?user_id=' . $query_user_id : '',
				'append_query_user' => $query_user_id ? '&user_id=' . $query_user_id : '',
			];
		}
	}
}