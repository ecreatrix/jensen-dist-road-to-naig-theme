<?php
namespace jg\Theme;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( HelpersUser::class ) ) {
    class HelpersUser {
        public function calculate_age( $dob ) {
            if ( '' === $dob ) {
                $age          = 0;
                $age_category = 'Undefined';
            } else {
                $birthyear = date( 'Y', strtotime( $dob ) );
                $age       = date( 'Y' ) - $birthyear;

                $age_category = '0-12';
                if ( $age >= 13 && $age <= 19 ) {
                    $age_category = '13-19';
                } else if ( $age >= 20 ) {
                    $age_category = '20+';
                }
            }

            return [
                'age'          => $age,
                'age_category' => $age_category,
            ];
        }

        static function complete_by_user( $user_id, $post_id, $register_link, $completion_key, $append = '', $complete_type = false ) {
            $family = self::get_family( $user_id );

            $edition = self::get_edition();

            $edition_info    = get_user_meta( $user_id, $edition, true );
            $user_completion = $edition_info && array_key_exists( $completion_key, $edition_info ) ? $edition_info[$completion_key] : false;

            $tag_link = '';
            if ( false && $register_link ) {
                $tag_link = ' href="' . $register_link . '" target="_blank"';
            }

            $text          = 'Register';
            $text_finished = 'Registered';
            if ( 'completed_training' === $completion_key ) {
                $text          = 'Complete';
                $text_finished = 'Completed';
            }

            $data_tags = ' data-type="' . $completion_key . '" data-post-id="' . $post_id . '" data-user-id="' . $user_id . '"';

            $points = 0;

            $complete_label = '';
            if ( $complete_type ) {
                $points = (int) get_theme_mod( $complete_type );

                if ( 'jg_points_off-ice' === $complete_type ) {
                    $complete_label = 'Off Ice';
                } else if ( 'jg_points_dryland' === $complete_type ) {
                    $complete_label = 'Dryland Training';
                } else if ( 'jg_points_challenges' === $complete_type ) {
                    $complete_label = 'Challenges';
                } else if ( 'jg_points_fitness_training' === $complete_type ) {
                    $complete_label = 'Fitness Training';
                }
            }

            if ( ! $family ) {
                if ( is_array( $user_completion ) && array_key_exists( $post_id, $user_completion ) ) {
                    $tag = '<div class="display-body has-primary-text-color">Already ' . $text_finished . '</div>';
                } else {
                    $tag = '<a class="register-' . $completion_key . ' btn btn-primary"' . $tag_link . $data_tags . ' data-points="' . $points . '" data-label="' . $complete_label . '">' . $text . $append . '</a>';
                }
            } else {
                $members = [];
                foreach ( $family as $key => $member ) {
                    $family[$key][$edition][$completion_key] = [];

                    if ( is_array( $member[$edition][$completion_key] ) && array_key_exists( $post_id, $member[$edition][$completion_key] ) ) {
                        $content = '<div class="display-body has-primary-text-color">' . $member['first_name'] . ' is ' . $text_finished . '</div>';
                    } else if ( $key ) {
                        $content = '<a class="register-' . $completion_key . ' btn btn-primary" ' . $data_tags . '" data-submember="' . $key . '"' . $tag_link . ' data-points="' . $points . '" data-label="' . $complete_label . '"' . $tag_link . '>' . $text . ' ' . $member['first_name'] . $append . '</a>';
                    }

                    $members[] = '<div class="member">' . $content . '</div>';
                }

                $tag = '<a class="btn btn-primary collapsed" data-bs-toggle="collapse" href="#collapseMembers' . $post_id . '" role="button" aria-expanded="false" aria-controls="collapseMembers' . $post_id . '"><span class="collapse-icon fa-stack"><i class="fas fa-circle fa-stack-2x"></i><i class="fas fa-caret-down fa-stack-1x"></i></span>Register</a><div class="members collapse" id="collapseMembers' . $post_id . '"> ' . implode( $members ) . ' </div>';
            }

            return $tag;
        }

        public function convert_summer_activities( $user_id ) {
            /*$edition = $this->get_edition();

        update_user_meta( $user_id, 'attended_workshops', [] );
        update_user_meta( $user_id, 'registered_workshops', [] );
        update_user_meta( $user_id, 'completed_activities', [] );
        update_user_meta( $user_id, 'completed_training', [] );
        update_user_meta( $user_id, 'points', 0 );

        $family = self::get_family( $user_id );
        if ( $family ) {
        foreach ( $family as $key => $member ) {
        $family[$key]['attended_workshops']   = [];
        $family[$key]['registered_workshops'] = [];
        $family[$key]['completed_activities'] = [];
        $family[$key]['completed_training']   = [];
        $family[$key]['points']               = 0;
        }

        update_user_meta( $user_id, 'family', $family );
        }*/
        }

        public function current_meta( $user_id ) {
            return array_map( function ( $a ) {
                return $a[0];
            }, get_user_meta( $user_id ) );
        }

        public function family_meta( $fields, $userdata ) {
            $user_id = wp_update_user( $userdata );

            $meta = $this->current_meta( $user_id );

            $account_type = $meta['form_type'] ? $meta['form_type'] : $fields['form_type']['value'];

            $meta = $this->userdata_individual( $fields, $meta );
            if ( 'parent' === $account_type ) {
                $meta = $this->userdata_individual( $fields, $meta, 'child1_' );
                $meta = $this->userdata_individual( $fields, $meta, 'child2_' );
                $meta = $this->userdata_individual( $fields, $meta, 'child3_' );
                $meta = $this->userdata_individual( $fields, $meta, 'child4_' );
            }

            foreach ( $fields as $key => $field ) {
                $value = $field['value'];

                if ( array_key_exists( $key, $meta ) ) {
                    update_user_meta( $user_id, $key, $value );
                }
            }
            update_user_meta( $user_id, 'form_type', $account_type );
            update_user_meta( $user_id, 'family', $meta['family'] );

            return $meta;
        }

        static function family_select_field( $user_id ) {
            $family = self::get_family( $user_id );
            if ( ! $family ) {
                return [];
            }

            $members = [];
            foreach ( $family as $key => $member ) {
                $members[$key] = $member['first_name'] . ' ' . $member['last_name'];
            }

            return $members;
        }

        public static function get_edition() {
            return get_theme_mod( 'jg_game_edition' ) ? get_theme_mod( 'jg_game_edition' ) : 'fall_2021';
        }

        static function get_family( $user_id ) {
            $account_type = get_user_meta( $user_id, 'form_type', true );

            if ( 'parent' === $account_type ) {
                $edition = self::get_edition();
                $family  = get_user_meta( $user_id, 'family', true );
                if ( is_array( $family ) ) {
                    unset( $family[""] );
                }
                foreach ( $family as $submember_key => $member ) {
                    if ( ! is_array( $member ) ) {
                        unset( $family[$submember_key] );
                    }
                }

                update_user_meta( $user_id, 'family', $family );

                /*if ( ! is_array( $family ) ) {
                // Family not array
                $family = [];
                }

                //unset( $family[$edition] );

                foreach ( $family as $submember_key => $member ) {
                if ( array_key_exists( $edition, $member ) ) {
                // Edition not added to submember yet, add it to every member
                $family[$submember_key][$edition] = self::member_info();
                }
                }

                update_user_meta( $user_id, 'family', $family );*/

                return $family;
            }

            return false;
        }

        public function get_field_value( $fields, $key, $profile_section = '' ) {
            $key = $profile_section . $key;

            if ( array_key_exists( $key, $fields ) ) {
                return $fields[$key]['value'];
            }

            return '';
        }

        public static function get_user_id() {
            $user_id = false;

            if ( is_user_logged_in() ) {
                if ( current_user_can( 'administrator' ) && array_key_exists( 'user_id', $_GET ) ) {
                    $user_id = $_GET['user_id'];
                } else {
                    global $current_user;

                    get_currentuserinfo();
                    $user_id = $current_user->ID;
                }
            }

            return $user_id;
        }

        static function member_info() {
            return [
                'points'               => 0,
                'completed_activities' => [],
                'uploads'              => [],
                'completed_training'   => [],
                'challenges'           => [],
                'registered_workshops' => [],
                'attended_workshops'   => [],
                'bonus_points'         => [],
            ];
        }

        // Update all accounts if admin
        public static function refresh_users( $fields, $action_keys, $user ) {
            $current_user = wp_get_current_user();

            if ( ! is_user_logged_in() || ! in_array( 'administrator', (array) $current_user->roles ) ) {
                return false;
            }

            $user_id = $user->ID;

            $email     = $fields[$action_keys['email']];
            $password  = $fields['jg_password_1626889919116'];
            $username  = $fields[$action_keys['username']];
            $form_type = $fields[$action_keys['form_type']];

            $account_type = get_user_meta( $user_id, 'form_type', true );

            if ( $user_id && ! in_array( 'administrator', (array) $user->roles ) ) {
                if ( ! $account_type || '' === $account_type ) {
                    update_user_meta( $user_id, 'form_type', $form_type );
                }

                update_user_meta( $user_id, 'show_admin_bar_front', 'false' );
                self::refresh_users_summer_editions( $user_id );

                $family = self::get_family( $user_id );
            }
            return false;
        }

        public static function refresh_users_summer_editions( $user_id ) {
            $account_type = $meta['form_type'][0] ? $meta['form_type'][0] : $fields['form_type']['value'];

            $meta   = get_user_meta( $user_id );
            $points = $meta['points'][0];

            $new_editions     = ['summer_2021', 'fall_2021'];
            $new_edition_meta = [];

            foreach ( $new_editions as $new_edition ) {
                $new_edition_meta[$new_edition] = self::member_info();
            }

            $family = self::get_family( $user_id );
            if ( $family ) {
                foreach ( $family as $key => $member ) {
                    $new_meta = [
                        'points'               => array_key_exists( 'points', $member ) ? $member['points'] : 0,
                        'completed_activities' => array_key_exists( 'completed_activities', $member ) ? $member['completed_activities'] : [],
                        'uploads'              => array_key_exists( 'uploads', $member ) ? $member['uploads'] : [],
                        'completed_training'   => array_key_exists( 'completed_training', $member ) ? $meta['completed_training'] : [],
                        'challenges'           => array_key_exists( 'challenges', $member ) ? $member['challenges'] : [],
                        'registered_workshops' => array_key_exists( 'registered_workshops', $member ) ? $member['registered_workshops'] : [],
                        'attended_workshops'   => array_key_exists( 'attended_workshops', $member ) ? $member['attended_workshops'] : [],
                        'bonus_points'         => array_key_exists( 'bonus_points', $member ) ? $member['bonus_points'] : [],
                    ];

                    if ( array_key_exists( $edition, $member ) ) {
                        $family[$key]['summer_2021'] = array_merge( $new_meta, $member['summer_2021'] );
                    } else {
                        $family[$key]['summer_2021'] = $new_meta;
                    }

                    unset( $family[$key]['points'] );
                    unset( $family[$key]['completed_activities'] );
                    unset( $family[$key]['uploads'] );
                    unset( $family[$key]['completed_training'] );
                    unset( $family[$key]['challenges'] );
                    unset( $family[$key]['registered_workshops'] );
                    unset( $family[$key]['attended_workshops'] );
                    unset( $family[$key]['bonus_points'] );
                }

                update_user_meta( $user_id, 'family', $family );
            }

            $new_meta = [
                'points'               => array_key_exists( 'points', $meta ) ? $meta['points'][0] : 0,
                'completed_activities' => array_key_exists( 'completed_activities', $meta ) ? unserialize( $meta['completed_activities'][0] ) : [],
                'uploads'              => array_key_exists( 'uploads', $meta ) ? unserialize( $meta['uploads'][0] ) : [],
                'completed_training'   => array_key_exists( 'completed_training', $meta ) ? unserialize( $meta['completed_training'][0] ) : [],
                'challenges'           => array_key_exists( 'challenges', $meta ) ? unserialize( $meta['challenges'] ) : [],
                'registered_workshops' => array_key_exists( 'registered_workshops', $meta ) ? unserialize( $meta['registered_workshops'][0] ) : [],
                'attended_workshops'   => array_key_exists( 'attended_workshops', $meta ) ? unserialize( $meta['attended_workshops'][0] ) : [],
                'bonus_points'         => array_key_exists( 'bonus_points', $meta ) ? unserialize( $meta['bonus_points'][0] ) : [],
            ];

            if ( ! array_key_exists( 'summer_2021', $meta ) ) {
                $new_edition_meta['summer_2021'] = $new_meta;
                update_user_meta( $user_id, 'summer_2021', $new_edition_meta['summer_2021'] );
            }

            delete_user_meta( $user_id, 'family_ignore' );
            delete_user_meta( $user_id, 'points' );
            delete_user_meta( $user_id, 'completed_activities' );
            delete_user_meta( $user_id, 'completed_training' );
            delete_user_meta( $user_id, 'challenges' );
            delete_user_meta( $user_id, 'registered_workshops' );
            delete_user_meta( $user_id, 'attended_workshops' );
            delete_user_meta( $user_id, 'bonus_points' );

            update_user_meta( $user_id, 'fall_2021', $new_edition_meta['fall_2021'] );
            update_user_meta( $user_id, 'editions', $new_editions );

        }

        public function update_password( $user_id, $username, $password ) {
            wp_set_password( $password, $user_id );

            if ( is_user_logged_in() ) {
                // Prevent user logout after password change
                wp_cache_delete( $user_id, 'users' );
                wp_cache_delete( $username, 'userlogins' );
                wp_logout();
                wp_signon( ['user_login' => $username, 'user_password' => $password] );
                wp_set_current_user( $user_id, $username );
                //wp_set_auth_cookie( $user_id );
            }
        }

        public function userdata_individual( $fields, $current_meta, $profile_section = '', $submember_slug = false ) {
            $firstname = $this->get_field_value( $fields, 'first_name', $profile_section );
            $lastname  = $this->get_field_value( $fields, 'last_name', $profile_section );

            $dob = $this->get_field_value( $fields, 'dob', $profile_section );
            $age = $this->calculate_age( $dob )['age'];

            $indigenous_ancestry = $this->get_field_value( $fields, 'indigenous_ancestry', $profile_section );
            $shirt_size          = $this->get_field_value( $fields, 'shirt_size', $profile_section );

            $edition  = $this->get_edition();
            $defaults = [$edition => [
                'points'               => 0,
                'completed_activities' => [],
                'uploads'              => [],
                'completed_training'   => [],
                'challenges'           => [],
                'registered_workshops' => [],
                'attended_workshops'   => [],
                'bonus_points'         => [],
            ]];

            if ( '' === $profile_section ) {
                // Main profile
                $new_meta = [
                    'form_type'           => $this->get_field_value( $fields, 'form_type' ),
                    'indigenous_ancestry' => $indigenous_ancestry,
                    'shirt_size'          => $shirt_size,
                    'dob'                 => $dob,
                    'age'                 => $age,

                    'street1'             => $this->get_field_value( $fields, 'street1' ),
                    'city1'               => $this->get_field_value( $fields, 'city1' ),
                    'postal_code1'        => $this->get_field_value( $fields, 'postal_code1' ),
                    'province_territory1' => $this->get_field_value( $fields, 'province_territory1' ),
                    'iswo_region'         => $this->get_field_value( $fields, 'iswo_region' ),
                    'how_did_hear'        => $this->get_field_value( $fields, 'how_did_hear' ),
                    'previous_experience' => $this->get_field_value( $fields, 'previous_experience' ),
                    'family'              => [],
                    'workshop_emails'     => [],
                ];

                return array_merge( $defaults, $current_meta, $new_meta );
            } else if ( '' !== $firstname ) {
                // Family member (subset of profile)
                $family = $current_meta['family'];
                if ( is_string( $family ) ) {
                    $family = unserialize( $family );
                }

                $slug = sanitize_title_with_dashes( $firstname . ' ' . $lastname );
                if ( $submember_slug && '' !== $submember_slug && array_key_exists( $submember_slug, $family ) ) {
                    // Get current submember data and remove from array
                    $existing_submember_meta = $family[$submember_slug];

                    unset( $family[$submember_slug] );
                    unset( $family[''] );

                    $slug = $submember_slug;

                    $new_meta = array_merge( $defaults, $existing_submember_meta, [
                        'first_name' => $firstname,
                        'last_name'  => $lastname,
                        'shirt_size' => $shirt_size,
                    ] );
                } else {
                    $existing_submember_meta = [];
                    $new_meta                = [
                        'first_name'          => $firstname,
                        'last_name'           => $lastname,
                        'indigenous_ancestry' => $indigenous_ancestry,
                        'shirt_size'          => $shirt_size,
                        'dob'                 => $dob,
                        'age'                 => $age,
                    ];
                }

                $family[$slug]          = array_merge( $defaults, $existing_submember_meta, $new_meta );
                $current_meta['family'] = $family;

                return $current_meta;
            }
        }

        public function userdata_profile( $fields, $user_id = false ) {
            $username = $this->get_field_value( $fields, 'username' );
            if ( is_user_logged_in() ) {
                $username = get_userdata( $user_id )->user_login;
            }

            $email = $this->get_field_value( $fields, 'email' );

            $firstname = $this->get_field_value( $fields, 'first_name' );
            $lastname  = $this->get_field_value( $fields, 'last_name' );

            $userdata = [
                'user_email'           => $email,
                'user_login'           => $username,
                'role'                 => 'subscriber',
                'show_admin_bar_front' => false,

                'first_name'           => $firstname,
                'last_name'            => $lastname,
                'display_name'         => $firstname . ' ' . $lastname,
            ];

            if ( is_user_logged_in() ) {
                $userdata['role'] = 'author';
            }

            return $userdata;
        }

        static function workshop_registration_email( $post_id ) {
            // Send one email per account per workshop signup
            $user_id = get_current_user_id();
            $emails  = get_user_meta( $user_id, 'workshop_emails', true );
            if ( ! $emails || ! is_array( $emails ) ) {
                $emails = [];
            }

            if ( ! array_key_exists( $post_id, $emails ) ) {
                global $current_user;
                $to      = $current_user->user_email;
                $subject = 'Registration: ' . get_the_title( $post_id );
                $message = apply_filters( 'the_content', 'Hello ' . get_user_meta( $user_id, 'first_name', true ) . ',<br><br>' . get_theme_mod( 'jg_workshop_email' ) );

                $status = wp_mail( $to, $subject, $message );

                if ( $status ) {
                    $emails[$post_id] = true;
                    update_user_meta( $user_id, 'workshop_emails', $emails );
                }
            }
        }
    }
}