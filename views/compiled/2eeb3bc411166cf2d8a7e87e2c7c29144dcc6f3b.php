<?php if(is_user_logged_in() || get_the_ID() !== (int) get_theme_mod( 'jg_signin' )): ?>
	<footer id="mastfooter" class="content-info" style="<?php echo jg\Theme\Helpers::mod_image( 'jg_footer_bg', true ); ?>">
		<div class="container">
			<div class="row">
				<div class="section contact col col-md-3">
					<?php echo jg\Theme\Helpers::mod_image( 'jg_footer_logo' ); ?>

					<?php echo jg\Theme\Helpers::address( ); ?>

					<?php echo jg\Theme\Helpers::phone( false, 'Phone: ' ); ?>

					<?php echo jg\Theme\Helpers::fax( false, 'Fax: ' ); ?>

				</div>
				<div class="section social col col-md-6">
					<div class="note">#ActivatingWellness</div>
					<?php echo jg\Theme\Helpers::add_social_media(); ?>

					<div class="row support">
						<div class="funded-by">
							<div>Funded by</div>
							<?php echo jg\Theme\Helpers::mod_image( 'jg_ontario_logo' ); ?>

						</div>
						<div class="sponsored-by">
							<div>Sponsored by</div>
							<?php echo jg\Theme\Helpers::mod_image( 'jg_sponsor_logo' ); ?>

						</div>
					</div>
				</div>
				<div class="section nav col col-md-3">
					<?php if(has_nav_menu('footer')): ?>
					<?php echo wp_nav_menu(['theme_location' => 'footer', 'menu_class' => 'navbar-nav']); ?>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</footer>
<?php endif; ?>
<?php /**PATH /Volumes/Files/_Business/Code/Projects/Jensen-all/Road to NAIG/Code/virtualgames/views/blade/partials/footer.blade.php ENDPATH**/ ?>