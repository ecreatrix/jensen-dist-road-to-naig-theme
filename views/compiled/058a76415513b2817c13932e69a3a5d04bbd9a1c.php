<form role="search" method="get" class="search-form" action="<?php echo e(home_url('/')); ?>">
  <label>
    <span class="screen-reader-text">
      <?php echo e(_x('Search for:', 'label', 'jg_theme')); ?>

    </span>

    <input
      type="search"
      class="search-field"
      placeholder="<?php echo esc_attr_x('Search &hellip;', 'placeholder', 'jg_theme'); ?>"
      value="<?php echo e(get_search_query()); ?>"
      name="s"
    >
  </label>

  <input type="submit" class="button" value="<?php echo e(esc_attr_x('Search', 'submit button', 'jg_theme')); ?>">
</form>
<?php /**PATH /Volumes/Files/_Business/Code/Projects/Jensen-all/Road to NAIG/Code/virtualgames/views/blade/forms/search.blade.php ENDPATH**/ ?>