<h3 class="text-tertiary">Workshop</h3>

<time class="updated" datetime="<?php echo e(get_post_time('c', true)); ?>">
  <?php echo e(get_post_meta(get_the_ID(), 'jg_timedate', true)); ?>

</time>

<p class="byline author vcard">
  <span><?php echo e(__('By', 'jg_theme')); ?></span>
  <a href="<?php echo e(get_author_posts_url(get_the_author_meta('ID'))); ?>" rel="author" class="fn">
    <?php echo e(get_the_author()); ?>

  </a>
</p>
<?php /**PATH /Volumes/Files/_Business/Code/Projects/Jensen-all/Road to NAIG/Code/roadtonaig/views/blade/partials/workshop-meta.blade.php ENDPATH**/ ?>