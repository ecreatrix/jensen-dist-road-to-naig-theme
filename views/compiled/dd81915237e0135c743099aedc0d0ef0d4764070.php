<header id="masthead">
	<nav class="navbar navbar-expand-lg navbar-light bg-faded">
		<?php if(is_user_logged_in() || get_the_ID() !== (int) get_theme_mod( 'jg_signin' )): ?>
			<div class="section register">
				<div class="container">
					<?php echo jg\Theme\Helpers::add_social_media(); ?>

					<?php if(has_nav_menu('top')): ?>
						<?php echo wp_nav_menu(['theme_location' => 'top', 'menu_class' => '']); ?>

					<?php endif; ?>
				</div>
			</div>

			<div class="section brand container">
				<a class="navbar-brand" href="<?php echo e(home_url('/')); ?>">
					<?php if(has_custom_logo()): ?>
						<?php echo jg\Theme\Helpers::mod_image( 'custom_logo' ); ?>

					<?php else: ?>
						<?php echo e(get_bloginfo('name', 'display')); ?>

					<?php endif; ?>
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
			<div class="section collapse navbar-collapse" id="navbarSupportedContent"><div class="container">
				<?php echo jg\Theme\Helpers::mod_image( 'jg_header_logo' ); ?>

				<?php if(has_nav_menu('primary')): ?>
					<?php echo wp_nav_menu(['theme_location' => 'primary', 'menu_class' => 'navbar-nav']); ?>

				<?php endif; ?>
			</div></div>
		<?php endif; ?>
	</nav>
</header><?php /**PATH /Volumes/Files/_Business/Code/Projects/Jensen-all/Road to NAIG/Code/virtualgames/views/blade/partials/header.blade.php ENDPATH**/ ?>