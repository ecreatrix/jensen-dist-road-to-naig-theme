<article @php(post_class())>
	<header>
		<div class="wp-block-jg-page-heading alignfull wp-block-cover page-heading has-secondary-background-color has-background-dim"><div class="wp-block-cover__inner-container"><h1 class="entry-title">Workshop</h1></div></div>
	</header>

<?php 
$post_id = get_the_ID();
$edition   = \jg\Theme\HelpersUser::get_edition();
$date_meta = strtotime( str_replace('/', '-', get_field( 'jg_timedate' ) ) );
$active_workshop = $date_meta > strtotime( 'now' ) ? true : false;

$recording_link = get_field( 'jg_recording' );
$register_link = get_field( 'jg_registration_link' );
$register_password = get_field( 'jg_registration_password' );
$resources = get_field( 'jg_resources' );
$presenters = get_field( 'jg_presenters');
$meeting_id = get_field( 'jg_meeting_id' );

$user_id = get_current_user_id();
if(current_user_can('administrator') && array_key_exists('user_id', $_GET)) {
	$user_id = $_GET['user_id'];
}

$registered_workshops = get_user_meta( $user_id, $edition , true)['registered_workshops'];
if(!is_array($registered_workshops)) {
	$registered_workshops = [];
}

// Show featured image unless workshop is over and user had registered
$user_registered = current_user_can('administrator') || ( is_user_logged_in() && array_key_exists($post_id, $registered_workshops) );
$content = has_post_thumbnail($post_id) ? '<h4 class="display-body">About the Workshop</h4>'.get_the_post_thumbnail( $workshop_id, 'large' ) : '';

if( !$active_workshop && $user_registered && $recording_link ) {
	$content = '<h4 class="display-body">Recorded Workshop</h4><div class="recording">'.apply_filters('the_content', $recording_link).'</div>';
}
?>

<div class="entry-content wp-block-cover has-white-background-color has-background-dim workshop-single"><div class="wp-block-cover__inner-container row">
	<div class="col-12 col-md-7 content">
		<div class="intro">
		    <h1 class="post-title">{!! $title !!}</h1>

			<div class="date row display-body">
	            @if( $date_meta )
	                <span class="col-12 col-md-6 day">Date: 
	                	<span class="has-primary-text-color">{{ date( 'jS F', $date_meta ) }}</span>
	                </span>
	             	@if( date( 'H:i A', $date_meta ) !== '00:00 AM' )
		             	<span class="col-12 col-md-6 time">Time: 
		                	<span class="has-primary-text-color">{{ date( 'H:i A', $date_meta ) }}</span>
		                </span>
	                @endif
	            @endif
            </div>
        </div><div class="text">
            @if( $content )
           		
           		{!! $content !!}
	        @endif

            @if( is_user_logged_in() && $active_workshop )
				{!! \jg\Theme\HelpersUser::complete_by_user( $user_id, $post_id, $register_link, 'registered_workshops', ' for Workshop' ) !!}
            @elseif( !is_user_logged_in() && $active_workshop )
            	Workshops are available for registered users. You can register by <a href="{{ \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'registration_start' )['permalink'] }}">clicking here</a>.
            @endif

            @if( $user_registered && $resources )
				<div class="resources black-bar">
					@foreach( $resources as $resource )
						@if( 'file' === $resource['acf_fc_layout'] )
							<a class="resource" href="{{ $resource['file']['url'] }}" download>
								<span class="title">{{ $resource['file']['title'] }}</span>
								<span class="fa-stack icon"><i class="fas fa-square fa-stack-2x"></i><i class="fas fa-download fa-stack-1x fa-inverse"></i></span>
							</a>
						@elseif( 'link' === $resource['acf_fc_layout'] )
							<a class="resource" href="{{ $resource['link'] }}" target="_blank">
								<span class="title">{{ $resource['link'] }}</span>
								<span class="fa-stack icon"><i class="fas fa-square fa-stack-2x"></i><i class="fas fa-download fa-stack-1x fa-globe"></i></span>
							</a>
						@endif
					@endforeach
		    	</div>
            @endif
		</div>
	</div>
	<div class="d-none d-md-block col-1 line"></div>
	<div class="col-12 col-md-4">
	        @if( $active_workshop && $user_registered )
	        	<div class="zoom-link">
	        		<h3 class="display-body">Zoom Meeting</h3>
	            	<a class="btn btn-primary" href="{{ $register_link }}" target="_blank">Launch Meeting</a>

		            @if( $register_password !== '' )
			        	<div class="password"><b>Password: </b>{{ $register_password }}</div>
		            @endif
		            @if( $meeting_id !== '' )
			        	<div class="password"><b>Meeting ID: </b>{{ $meeting_id }}</div>
		            @endif
		        </div>
	        @endif

		<div class="presenters">
			@if( $presenters )
		        <h3 class="display-body">Presented by:</h3>

		        @if( count($presenters)===0 || $presenters[0]['jg_presenter_name'] === '' )
					<div class="presenter">
						<div class="name display-body">TBA</div>
					</div>
		        @endif

				@foreach( $presenters as $presenter )
					<div class="presenter">
						@php ( $thumb = $presenter['jg_picture'] )
		            	@if( $thumb )
		            		<div class="image">{!! wp_get_attachment_image($thumb, 'large') !!}</div>
		            	@endif

						<div class="name display-body">{{ $presenter['jg_presenter_name'] }}</div>

						@php ( $email = $presenter['jg_presenter_email'] )
		            	@if( $email )
		            		<div class="email"><a class="btn btn-link display-body" href="mailto:{{ $email }}">{{ $email }}</a></div>
		            	@endif

						<div class="info">{{ $presenter['jg_presenter_info'] }}</div>
					</div>
		    	@endforeach
		    @endif
		</div>
	</div>
</div>
</article>
