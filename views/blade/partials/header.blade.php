<header id="masthead">
	<nav class="navbar navbar-expand-lg navbar-light bg-faded">
		@if (is_user_logged_in() || get_the_ID() !== (int) get_theme_mod( 'jg_signin' ))
			<div class="section account-bar">
				<div class="container">
					{!! jg\Theme\HelpersSocial::add_social_media() !!}
					@if (has_nav_menu('top'))
						{!! wp_nav_menu(['theme_location' => 'top', 'menu_class' => '']) !!}
					@endif
				</div>
			</div>

			<div class="section brand container">
				<a class="navbar-brand" href="{{ home_url('/') }}">
					@if (has_custom_logo())
						{!! \jg\Theme\HelpersTheme::mod_image( 'custom_logo' ) !!}
					@else
						{{ get_bloginfo('name', 'display') }}
					@endif
				</a>

				<button class="navbar-toggler" type="button" data-bs-target="#navbarSupportedContent"  data-bs-toggle="collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			</div>
			<div class="section collapse navbar-collapse" id="navbarSupportedContent"><div class="container">
				{!! \jg\Theme\HelpersTheme::mod_image( 'jg_header_logo' ) !!}
				@if (has_nav_menu('primary'))
					{!! wp_nav_menu(['theme_location' => 'primary', 'menu_class' => 'navbar-nav']) !!}
				@endif
			</div></div>
		@endif
	</nav>
</header>